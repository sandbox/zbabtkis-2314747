;(function(exports, $) {
	var Accounts = {};

	// Account config
	Accounts.INSTAGRAM = 30435276;

	$(function() {

		new exports.SocialWall(
			document.getElementById("social-wall"),
			[
				SocialWall.Social.Instagram(Accounts.INSTAGRAM)
			]
		).run();

	});
}).call(this, this, jQuery);
